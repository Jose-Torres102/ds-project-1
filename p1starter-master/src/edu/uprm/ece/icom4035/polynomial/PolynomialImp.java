package edu.uprm.ece.icom4035.polynomial;

import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.ece.icom4035.list.List;

public class PolynomialImp implements Polynomial {
	
	private class PolynomialImpIterator<Term> implements Iterator<Term>{
		
		private Term currentTerm;
		int index = 0;
		
		public PolynomialImpIterator() {
			this.currentTerm = (Term) L.get(0);
		}
		
		@Override
		public boolean hasNext() {
			if(this.currentTerm == null) return false;
			return true;
		}

		@Override
		public Term next() {
			if (this.hasNext()) {
				Term result = null;
				result = this.currentTerm;
				if(this.currentTerm.equals(L.last())) {
					this.currentTerm = null;
				}
				else
				this.currentTerm = (Term) L.get(++index);
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}
	
	private List<Term> L;

	public PolynomialImp(String string) {
		this.L = this.addTerms(string);
	}
	
	public PolynomialImp(List<Term> list) {
		this.L = list;
	}
	
	
	private List<Term> addTerms(String string){
		List<Term> L = new TermListFactory().newListFactory().newInstance();
		String arr[] = string.split("[+]");
		for (int i = 0; i < arr.length; i++) {
			String arr2[] = arr[i].split("[x^ ]");
			int length = arr[i].length();
			if(((arr[i].charAt(0) == 'x' || Character.isLetter(arr[i].charAt(0)))  && arr2.length > 1)) L.add((Term) new TermImp(1.0, Integer.valueOf(arr2[arr2.length-1])));
			else if (arr2.length == 0) L.add(new TermImp(1.0, 1));
			else if((Character.isLetter(arr[i].charAt(length-1)))) L.add((Term) new TermImp((double) Double.valueOf(arr2[0]),(int) 1 ));
			else if(arr2.length == 1) L.add((Term) new TermImp((double) Double.valueOf(arr2[0]),(int) 0 ));
			else
			L.add((Term) new TermImp((double) Double.valueOf(arr2[0]), (int) Integer.valueOf(arr2[2])));
		}
		return L;
	}
	
	public Term getFirstTerm() {
		return L.first();
	}

	@Override
	public Iterator<Term> iterator() {
		return new PolynomialImpIterator<Term>();
	}

	@Override
	public Polynomial add(Polynomial P2) {
		
		List<Term> L2 = new TermListFactory().newListFactory().newInstance();
        for (Term term : P2) {
			L2.add(term);
		}
        
        for (Term term : this) {
			L2.add(term);
		}
        
        Simplify(L2);
        insertionSort(L2);
		return new PolynomialImp(L2);
	}

	@Override
	public Polynomial subtract(Polynomial P2) {
		if(this.equals(P2)) return new PolynomialImp("0");
		return this.add(P2.multiply(-1));
	}

	@Override
	public Polynomial multiply(Polynomial P2) {
		if(P2.equals(new PolynomialImp("0"))) return  new PolynomialImp("0");
		
		List<Term> L3 = new TermListFactory().newListFactory().newInstance();
		for (Term term : this) {
			for (Term term2 : P2) {
			L3.add(new TermImp(term.getCoefficient()*term2.getCoefficient()
					, term.getExponent()+term2.getExponent()));
			}
		}
		
		
		Simplify(L3); 
	    insertionSort(L3);
		return new PolynomialImp(L3);
	}

	@Override
	public Polynomial multiply(double c) {
		if(c == 0) return new PolynomialImp("0");
		
		if( c == 1) return this;
		
		List<Term> L2 = this.L;	
		for (int i = 0; i < L2.size(); i++) {
			L2.set(i, new TermImp(L2.get(i).getCoefficient()*c, L2.get(i).getExponent()));
		}
		return new PolynomialImp(L2);
	}

	@Override
	public Polynomial derivative() {
		if(this.degree() == 0) return new PolynomialImp("0");
		else {
			for (int i = 0; i < this.L.size(); i++) {
				int newExponent = (L.get(i).getExponent()-1);
				double newValue = L.get(i).getCoefficient()*(newExponent+1);
				if(L.get(i).getExponent() != 0) L.set(i, new TermImp(newValue, newExponent));
				else this.L.remove(i);
			}
		}
		return this;
	}

	@Override
	public Polynomial indefiniteIntegral() {

		for (int i = 0; i < L.size(); i++) {
			int newExponent = (L.get(i).getExponent()+1);
			double newValue = L.get(i).getCoefficient()/newExponent;
			L.set(i, new TermImp(newValue, newExponent));
		}
		if(this.L.last().getExponent() != 0) L.add(new TermImp(1, 0));
		
		return this;
	}

	@Override
	public double definiteIntegral(double a, double b) {
		Polynomial temp = this.indefiniteIntegral();
		Double valueA = temp.evaluate(a);
		Double valueB = temp.evaluate(b);
		return valueB - valueA;
	}

	@Override
	public int degree() {
		return this.getFirstTerm().getExponent();
	}

	@Override
	public double evaluate(double x) {
		double result = 0.0;
        for (int i = 0; i < this.L.size(); i++) {
			result += L.get(i).getCoefficient()*Math.pow(x, L.get(i).getExponent());
		}
		return result;
	}

	@Override
	public boolean equals(Polynomial P) {
		return this.toString().equals(P.toString());
	}
	
	public String toString() {
		List<Term> L2 = new TermListFactory().newListFactory().newInstance();
		for (Term term : this) {
			L2.add(term);
		}
		String result = String.format("%.2f",L2.get(0).getCoefficient())+"x^"+L2.get(0).getExponent();
		if(this.degree() == 1 && L2.size() == 1) return String.format("%.2f",L.get(0).getCoefficient())+"x";
		else if(this.degree() == 1 && L2.size() == 2) return String.format("%.2f",L.get(0).getCoefficient())+"x"+String.format("%.2f", L.get(1).getCoefficient());
		if(this.degree() == 0) return String.format("%.2f",L.get(0).getCoefficient());
		else if(this.degree()>1) {

		for (int i = 1; i < this.L.size(); i++) {
			if(L2.get(i).getExponent() == 1) result += "+" + String.format("%.2f",L2.get(i).getCoefficient())+"x";
			else if(L2.get(i).getExponent() == 0) result += "+" + String.format("%.2f",L2.get(i).getCoefficient());
			else result += "+" + String.format("%.2f",L2.get(i).getCoefficient())+"x^"+ L2.get(i).getExponent();
		}

		}
		return result;
	}
	
	
//	public List<Term> bubbleSort(List<Term> list){ // Organiza la lista de mayor
//		                                      // exponente a menor excepto que lento cc
//		for (int i = 0; i < list.size(); i++) {
//			for (int j = i+1; j < list.size(); j++) {
//				if(list.get(i).getExponent() < list.get(j).getExponent()) {
//					Term temp = list.get(j);
//					list.set(j, list.get(i));
//					list.set(i, temp);
//				}
//			}
//		}
//		return list;
//	}
	
	public List<Term> insertionSort(List<Term> list){ // Organiza la lista de mayor
        												// exponente a menor
			for (int i = 1; i < list.size(); i++) {
				for (int j = i; j > 0; j--) {
					if(list.get(j).getExponent() > list.get(j-1).getExponent()) {
						Term temp = list.get(j);
						list.set(j, list.get(j-1));
						list.set(j-1, temp);
					}
				}
			}
			return list;
	}
	
	public List<Term> Simplify(List<Term> L2){      //sumar todos los terminos con grado igual
	      for (int i = 0; i < L2.size(); i++) {    // y eliminar los coeficientes 0
				for (int j = i+1; j < L2.size(); j++) {
					if(L2.get(i).getExponent() == L2.get(j).getExponent()) {
						L2.set(i, new TermImp(L2.get(i).getCoefficient()+L2.get(j).getCoefficient()
								, L2.get(i).getExponent()));
					L2.remove(j);
					}
				}
			}
	      for (int i = 0; i < L2.size(); i++) {
			if(L2.get(i).getCoefficient() == 0) L2.remove(i);
		}
	      return L2;
	}
	
}

