package edu.uprm.ece.icom4035.polynomial;

public class TermImp implements Term {

	public double coefficient;
	public int exponent;
	
	public TermImp(double coefficient, int exponent) {
		this.coefficient = coefficient;
		this.exponent = exponent;
	}
	
	
	@Override
	public double getCoefficient() {
		return this.coefficient;
	}

	@Override
	public int getExponent() {
		return this.exponent;
	}

	@Override
	public double evaluate(double x) {
		return this.coefficient*Math.pow(x, this.exponent);
	}

}
