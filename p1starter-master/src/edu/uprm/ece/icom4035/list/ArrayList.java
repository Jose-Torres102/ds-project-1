package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayList<E> implements List<E> {
	
	private class ArrayListIterator<E> implements Iterator<E>{
		private int currentPosition;
		
		public ArrayListIterator(){
			this.currentPosition = 0;
		}
		@Override
		public boolean hasNext() {
			return this.currentPosition < size();
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = null;
				result = (E) elements[this.currentPosition++];
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
	}

	private static final int INITCAP = 5;
	private E elements[];
	private int size;
	
	public ArrayList() {
		this.elements= (E[]) new Object[INITCAP];
		this.size = 0;
	}
	public ArrayList(int initCapacity) {
		if (initCapacity < 1) {
			throw new IllegalArgumentException
			("Initial capacity must be at least 1");
		}
		this.elements= (E[]) new Object[initCapacity];
		this.size = 0;
	}	
	
	
	@Override
	public Iterator<E> iterator() {
		return new ArrayListIterator<E>();
	}
	
	private E[] changeCapacity(int i) {
		E[] temp = (E[]) new Object[i*2];
		for (int j = 0; j < elements.length; j++) {
			temp[j] = elements[j];
		}
		elements = temp;
		
		return elements;
	}
	
	@Override
	public void add(E obj) {
		if(this.size == elements.length) this.changeCapacity(this.size);

 elements[this.size++] = obj;
	}

	@Override
	public void add(int index, E obj) {
		if(index == this.size) add(obj);
		if(index > this.size || index < 0) throw new IndexOutOfBoundsException();
		if(this.size == elements.length) this.changeCapacity(this.size);
			
			for (int j = this.size; j > index; j--) {
				elements[j] = elements[j-1];
			}
			this.set(index, obj);
			this.size++;
	}
	
	private int findElementPosition(E obj) {
		for (int i = 0; i < this.size; i++) {
			if(elements[i].equals(obj)) return i;
		}
		return -1;
	}

	@Override
	public boolean remove(E obj) {
		int position = this.findElementPosition(obj);
		if(position != -1) {
			return this.remove(position);
		}

		return false;
	}

	@Override
	public boolean remove(int index) {
		if(index < 0 || index > this.size) throw new IndexOutOfBoundsException();

			for (int i = index; i < this.size - 1; i++) {
				elements[i] = elements[i+1];
			}
			elements[this.size - 1] = null;
			size--;
			return true;
			
	}

	@Override
	public int removeAll(E obj) {
		int count = 0;
		while(this.remove(obj)) count++;
		size -= count;
		return count;
	}

	@Override
	public E get(int index) {
		if (index < 0  || index >= this.size) throw new IndexOutOfBoundsException();
		
		return elements[index];
	}

	@Override
	public E set(int index, E obj) {
		if(index < 0 || index > this.size) throw new IndexOutOfBoundsException();

		E result = elements[index];
		elements[index] = obj;
		return result;
	}

	@Override
	public E first() {
		if(this.isEmpty()) return null;
		
		return elements[0];
	}

	@Override
	public E last() {
		if(this.isEmpty()) return null;
		
		return elements[this.size-1];
	}

	@Override
	public int firstIndex(E obj) {
		return this.findElementPosition(obj);
	}

	@Override
	public int lastIndex(E obj) {
		int lastPosition = 0;
		
		for (int i = 0; i < this.size; i++) {
			if(elements[i].equals(obj) && i > lastPosition) lastPosition = i;
		}
		return lastPosition;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public boolean contains(E obj) {
		if(this.findElementPosition(obj) != -1) return true;
		
		return false;
	}

	@Override
	public void clear() {
		for (int i = 0; i < this.size; i++) {
			elements[i] = null;
		}
		
		this.size = 0;
	}

}
