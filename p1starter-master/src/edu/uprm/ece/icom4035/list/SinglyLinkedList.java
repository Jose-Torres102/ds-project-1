package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SinglyLinkedList<E> implements List<E> {
	
	private static class Node<E>{
		
		private E element;
		private Node<E> next;
		
		public Node(){
			this.element = null;
			this.next = null;
		}
		
		public Node(E e, Node<E> N) {
			this.element = e;
			this.next = N;
		}

		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}
	}
		private class SinglyLinkedListIterator<E> implements Iterator<E>{
			
			private Node<E> currentNode;
			
			public SinglyLinkedListIterator() {
				this.currentNode = (Node<E>) header.getNext();
			}
			
			@Override
			public boolean hasNext() {
				return this.currentNode != null;
			}

			@Override
			public E next() {
				if (this.hasNext()) {
					E result= null;
					result = this.currentNode.getElement();
					this.currentNode = this.currentNode.getNext();
					return result;
				}
				else {
					throw new NoSuchElementException();
				}
			}
			
		}
		
		private Node<E> header;
		private int size;
		
		public SinglyLinkedList() {
			this.header = new Node<E>();
			this.size = 0;
		}

	@Override
	public Iterator<E> iterator() {
		return new SinglyLinkedListIterator<E>();
	}
	

	@Override
	public void add(E obj) {
		 Node<E> newNode = new Node<E>(obj, null);
		 
		if(this.isEmpty()) {
			this.header.setNext(newNode);
			this.size++;
		}
		else {
		 this.findNode(this.size-1).setNext(newNode);
		 this.size++;
		}
	}

	@Override
	public void add(int index, E obj) {
		Node<E> newNode = new Node<E>(obj, null);
		if(index < 0 || index > this.size) throw new IndexOutOfBoundsException();
		if(index == this.size) this.add(obj);
		else if(this.isEmpty()) {
			this.header.setNext(newNode);
			this.size++;
		}
		else if(index == 0) {
			newNode.setNext(header.getNext());
			header.setNext(newNode);
			this.size++;
		}
		else {
			newNode.setNext(this.findNode(index));
			this.findNode(index - 1).setNext(newNode);
			this.size++;
		}
	}

	@Override
	public boolean remove(E obj) {
		if(!this.contains(obj)) return false;
		else
		return this.remove(this.findIndex(obj));
	}

	@Override
	public boolean remove(int index) {
		if(index < 0 || index >= this.size) throw new IndexOutOfBoundsException();
		Node<E> target = this.findNode(index);
		if(index == 0) {
	      this.header.setNext(this.findNode(index + 1));
	      target.setNext(null);
	      target.setElement(null);
	      this.size--;
	      return true;
		}
		if(this.contains(target.getElement())) {
			this.findNode(index-1).setNext(this.findNode(index+1));
			target.setElement(null);
			target.setNext(null);
			this.size--;
			return true;
		}
		return false;
	}

	@Override
	public int removeAll(E obj) {
		int count = 0;
		for (int i = 0; i < this.size; i++) {
			if(this.remove(obj)) {
				this.remove(obj);
				count++;
			}
		}
		return count;
	}

	@Override
	public E get(int index) {
		if(index < 0 || index >= this.size) throw new IndexOutOfBoundsException();
		return this.findNode(index).getElement();
	}

	@Override
	public E set(int index, E obj) {
		if(index < 0 || index >= this.size) throw new IndexOutOfBoundsException();
		
		E result = this.findNode(index).getElement();
		this.findNode(index).setElement(obj);
		return result;
	}

	@Override
	public E first() {
		return this.header.getNext().getElement();
	}

	@Override
	public E last() {

		return this.get(this.size-1);
	}

	@Override
	public int firstIndex(E obj) {

		return this.findIndex(obj);
	}

	@Override
	public int lastIndex(E obj) {
		int lastPosition = 0;
		
		for (int i = 0; i < this.size; i++) {
			if(this.get(i).equals(obj) && i > lastPosition) lastPosition = i;
		}
		return lastPosition;
	}

	@Override
	public int size() {

		return this.size;
	}

	@Override
	public boolean isEmpty() {

		return this.size == 0;
	}

	@Override
	public boolean contains(E obj) {
		return this.findIndex(obj) != -1;
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
		
	}
	
	private Node<E> findNode(int index) {
		Node<E> temp = header.getNext();
		for (int i = 0; i < index; i++) {
			temp = temp.getNext();
		}
		return temp;
	}
	
	private int findIndex(E obj) {
		Node<E> temp = header.getNext();
		for (int i = 0; i < this.size; i++) {
			if(temp.getElement().equals(obj)) return i;
			else
				temp = temp.getNext();
		}
		return -1;
	}
}